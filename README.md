[Website is live click here to visit live version](https://mydogcare.netlify.app/)

[Figma design link is here which I used to make the web interface](https://www.figma.com/file/UlJhl9OsA188tqqoJM9oOV/MyDogCare---HTML-%26-CSS?node-id=93%3A0)

Destop design
<hr>
<img src="/assets/UI Screenshots/desktop-version.png" alt="desktop-version screenshot" >

Tablet version
<hr>
<img src="/assets/UI Screenshots/tab-version.png" alt="tab-version screenshot" >
